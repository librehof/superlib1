#!/bin/sh
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
. "${SCRIPTPATH}/superlib"
cd "${SCRIPTPATH}"

#-------------------------------------------------

# box/run

summary()
{
  info ""
  info "Run test"
  info ""
  info "Usage:"
  info " ./${SCRIPT} <boxdir>|all [<test>]"
  info ""
  info "test => run test"
  info "no test => ssh prompt (manual testing)"
  info ""
  info "If the file sudo.list contains name of test"
  info "then it will be run using sudo"
  info ""
}

#-------------------------------------------------

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit
fi

BOX="$(basename "${1}")"
TEST="$(basename "${2}")"

if [ "${BOX}" = "" ]; then
  inputexit
fi

if [ "${TEST}" != "" ]; then
  checkfile "../${TEST}"
  exitonerror ${?}
fi

MODE="user"
if [ -f "sudo.list" ]; then
  FOUND="$(grep "${TEST}" "sudo.list")"
  if [ "${FOUND}" != "" ]; then
    MODE="superuser"
  fi
fi

#-------------------------------------------------

# all

if [ "${BOX}" = "all" ]; then

  if [ "${TEST}" = "" ]; then
    errorexit "No test specified"
  fi

  rm -rf "${RUNPATH}/errors.log"

  LIST="$(ls -1 "./")"
  for BOX in ${LIST}; do
    if [ -f "${BOX}/Vagrantfile" ]; then
      "${SCRIPTPATH}/run" "${BOX}" "${TEST}"
    fi
  done

  if [ -f "${RUNPATH}/errors.log" ]; then
    info
    note "Errors"
    cat "${RUNPATH}/errors.log"
    exit 2
  else
    info
    note "No errors"
    exit 0
  fi

fi

#-------------------------------------------------

# one

if [ ! -d "${BOX}" ]; then
  errorexit "Missing ${BOX}"
fi

cd "${BOX}"

if [ "${TEST}" = "" ]; then

  info
  note "*** ${BOX}"
  info

  vagrant ssh

else

  INFO="${TEST} @ ${BOX}"

  info
  note "*** ${INFO} ($(hosttime "%H:%M:%S"))"
  info

  vagrant up
  "${SCRIPTPATH}/copy" "${BOX}"

  note "Prepared"

  if [ "${MODE}" = "superuser" ]; then
    vagrant ssh -c "sudo test/${TEST}"
    STATUS=${?}
  else
    vagrant ssh -c "test/${TEST}"
    STATUS=${?}
  fi

  info
  END="${INFO} ($(hosttime "%H:%M:%S"))"
  if [ ${STATUS} != 0 ]; then
    note "FAILED: ${END}"
    echo "FAILED: ${END}" >> "${RUNPATH}/errors.log"
    sleep 1
  else
    note "OK: ${END}"
  fi
  info
  exit ${STATUS}

fi

#-------------------------------------------------
