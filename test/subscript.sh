#!/bin/sh
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SUPERPATH="${SCRIPTPATH}/../bin"
. "${SUPERPATH}/superlib"
cd "${SCRIPTPATH}"

#-------------------------------------------------

# superlib1 | subscript

title

#-------------------------------------------------
separator "grand"

note "sub: GRANDDEPTH=${GRANDDEPTH}"
note "sub: GRANDPID=${GRANDPID}"

div

#-------------------------------------------------
separator "other"

note "sub: SCRIPTPATH=${SCRIPTPATH}"
note "sub: SCRIPTNAME=${SCRIPTNAME}"
note "sub: LOGFILE=${LOGFILE}"
note "sub: STDFILE=${STDFILE}"
note "sub: DIV=${DIV}"

# write to stdfile
echo "sub" > "${STDFILE}"

#-------------------------------------------------
separator "end"
