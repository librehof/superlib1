#!/bin/sh
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SUPERPATH="${SCRIPTPATH}/../bin"
. "${SUPERPATH}/superlib"
cd "${SCRIPTPATH}"

#-------------------------------------------------

# superlib1 | test-superlib

summary()
{
  info
  info "Test \"most\" superlib functions"
  info
  info "Usage:"
  info " ${SCRIPT}"
  info
}

#-------------------------------------------------

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit
fi

title

TESTPATH="${RUNPATH}/superlib"
rm -rf "${TESTPATH}"
ensuredir "${TESTPATH}"
ensurelog "${TESTPATH}/test.log"

#-------------------------------------------------

checklog()
{
  LAST="$(tail -1 "${LOGFILE}" | grep "${1}")"
  exitonerror ${?} "failed to check ${LOGFILE}"
  if [ "${LAST}" = "" ]; then
    errorexit "checklog: expected: ${1}"
  fi
}

#-------------------------------------------------
separator "variables"

note "SUPERLIB=${SUPERLIB}"

div

note "SCRIPTPATH=${SCRIPTPATH}"
note "SCRIPTUNIT=${SCRIPTUNIT}"
note "SCRIPT=${SCRIPT}"
note "SCRIPTNAME=${SCRIPTNAME}"
note "SCRIPTPID=${SCRIPTPID}"

div

note "RUNPATH=${RUNPATH}"
note "TMPPATH=${TMPPATH}"
note "LOGFILE=${LOGFILE}"
note "LOGLIMIT=${LOGLIMIT}"
note "TIMEOUT=${TIMEOUT}"
note "SUPERPATH=${SUPERPATH}"

div

note "GRANDDEPTH=${GRANDDEPTH}"
note "GRANDPID=${GRANDPID}"
note "GRANDPATH=${GRANDPATH}"
note "GRANDSCRIPT=${GRANDSCRIPT}"
note "GRANDNAME=${GRANDNAME}"
note "GRANDUID=${GRANDUID}"
note "GRANDUSER=${GRANDUSER}"
note "LOGDAY=${LOGDAY}"

div

note "UID=${UID}"
note "USER=${USER}"
note "DIV=${DIV}"
note "STDFILE=${STDFILE}"
note "MIB=${MIB}"
note "TTYSTRING=${TTYSTRING}"

#-------------------------------------------------
separator "key"

info "Will continue in 5 seconds, press CTRL-C to abort"
keywait 5

info "Press key \"x\" in 5 seconds"
KEY="$(keyinput 5)"
if [ "${KEY}" = "" ]; then
  note "No key (timeout) or ENTER was pressed"
elif [ "${KEY}" = "x" ]; then
  note "Key x was pressed"
else
  errorexit "Key ${KEY} was pressed"
fi
sleep 0.5

#-------------------------------------------------
separator "time, filelog and printlog"

DAY="$(utcday)"
if [ "$(stringlen "${DAY}")" != 8 ]; then
  errorexit "utcday"
fi
note "utcday: ${DAY}"

TIME="$(utctime)"
if [ "$(stringlen "${TIME}")" != 15 ]; then
  errorexit "utctime"
fi
note "utctime: ${TIME}"

DAY="$(hostday)"
if [ "$(stringlen "${DAY}")" != 8 ]; then
  errorexit "hostday"
fi
note "hostday: ${DAY}"

TIME="$(hosttime)"
if [ "$(stringlen "${TIME}")" != 15 ]; then
  errorexit "hosttime"
fi
note "hosttime: ${TIME}"

info "Press ENTER if correct time, CTRL-C if not"
keywait 10

clearfile "${TESTPATH}/file.log"
ensurenofile "${TESTPATH}/file.log.old"
COUNT=10
while [ "${COUNT}" != 0 ]; do
  filelog "1234567890abcdef" "${TESTPATH}/file.log" 100
  COUNT=$((COUNT-1))
done
if [ ! -f "${TESTPATH}/file.log.old" ]; then
  errorexit "filelog rotation"
fi

printlog "testing"
checklog "testing"

clearfile "${STDFILE}"
echo "someerror" >> "${STDFILE}"
notetail "${STDFILE}"
checklog "someerror"

# multi-line
lines="$(printf '%s\n%s' "line1" "line2")"
note "${lines}"
checklog "line2"
lines="$(printf '%s\n%s\n' "line3" "line4")"
note "${lines}"
checklog "line4"

#-------------------------------------------------
separator "reporting"

action "Doing something"
checklog "Doing something"

note "This is a note"
warning "This is a warning"
error "This is an error"

success
checklog "ok"

#-------------------------------------------------
separator "string"

STR="$(startstring "abc123" 3)"
if [ "${STR}" != "abc" ]; then
  errorexit "startstring1"
fi

STR="$(startstring "abc123" 1)"
if [ "${STR}" != "a" ]; then
  errorexit "startstring2"
fi

STR="$(startstring "abc123" 0)"
if [ "${STR}" != "" ]; then
  errorexit "startstring3"
fi

#------------

STR="$(endstring "abc123" 7)"
if [ "${STR}" != "" ]; then
  errorexit "endstring1"
fi

STR="$(endstring "abc123" 6)"
if [ "${STR}" != "3" ]; then
  errorexit "endstring2"
fi

STR="$(endstring "abc123" 4)"
if [ "${STR}" != "123" ]; then
  errorexit "endstring3"
fi

#------------

STR="$(upperstring "Abc123")"
if [ "${STR}" != "ABC123" ]; then
  errorexit "upperstring"
fi

STR="$(lowerstring "Abc123")"
if [ "${STR}" != "abc123" ]; then
  errorexit "lowerstring"
fi

#------------

STR="abcxyz123"
findforward "${STR}" "xyz"
if [ "${?}" != 0 ]; then
  errorexit "findforwardI"
fi
if [ "${found}" != "abc" ]; then
  errorexit "findforwardI"
fi

STR="abcxyz123xyz456"
findforward "${STR}" "xyz" 7
if [ "${?}" != 0 ]; then
  errorexit "findforwardII"
fi
if [ "${found}" != "123" ]; then
  info "${found}"
  errorexit "findforwardII"
fi

STR="abc.123"
findforward "${STR}" "."
if [ "${?}" != 0 ]; then
  errorexit "findforward1"
fi
if [ "${found}" != "abc" ]; then
  errorexit "findforward2"
fi

STR="abc.123"
findforward "${STR}" "x"
if [ "${?}" = 0 ]; then
  errorexit "findforward3"
fi

STR=".abc123"
findforward "${STR}" "."
if [ "${?}" != 0 ]; then
  errorexit "findforward4"
fi
if [ "${found}" != "" ]; then
  errorexit "findforward5"
fi
if [ "${pos}" != 0 ]; then
  errorexit "findforward6"
fi
PART="$(substring "${STR}" ${pos} ${end})"
if [ "${PART}" != "" ]; then
  errorexit "findforward7"
fi

#------------

STR="abc.123"
findbackward "${STR}" "."
if [ "${?}" != 0 ]; then
  errorexit "findbackward1"
fi
if [ "${found}" != "123" ]; then
  errorexit "findbackward2"
fi

STR="abc123."
findbackward "${STR}" "."
if [ "${?}" != 0 ]; then
  errorexit "findbackward3"
fi
if [ "${found}" != "" ]; then
  errorexit "findbackward4"
fi
if [ "${pos}" != 8 ]; then
  errorexit "findbackward5"
fi
PART="$(substring "${STR}" ${pos} ${end})"
if [ "${PART}" != "" ]; then
  errorexit "findbackward6"
fi

STR="abc.123"
findbackward "${STR}" "x"
if [ "${?}" = 0 ]; then
  errorexit "findbackward7"
fi

#-------------------------------------------------
separator "field"

LINE="$(printf "a1  b2")"
FIELD1="$(spacefield "${LINE}" 1)"
FIELD2="$(spacefield "${LINE}" 2)"
FIELD3="$(spacefield "${LINE}" 3)"
if [ "${FIELD1}" != "a1" ] || [ "${FIELD2}" != "b2" ] || [ "${FIELD3}" != "" ]; then
  errorexit "spacefield"
fi

LINE="$(printf "a1\tb2")"
FIELD1="$(tabfield "${LINE}" 1)"
FIELD2="$(tabfield "${LINE}" 2)"
FIELD3="$(tabfield "${LINE}" 3)"
if [ "${FIELD1}" != "a1" ] || [ "${FIELD2}" != "b2" ] || [ "${FIELD3}" != "" ]; then
  errorexit "tabfield"
fi

#-------------------------------------------------
separator "replace"

STR="abc123"
NEWSTR="$(replaceinstring "abc" "cba" "${STR}")"
if [ "${NEWSTR}" != "cba123" ]; then
  errorexit "replaceinstring"
fi

echo "abc123" > "${TESTPATH}/test.txt"
replaceinfile "abc" "cba" "${TESTPATH}/test.txt"
NEWSTR="$(cat "${TESTPATH}/test.txt")"
if [ "${NEWSTR}" != "cba123" ]; then
  errorexit "replaceinfile"
fi

#-------------------------------------------------
separator "size"

BYTES=$(bytesize "123")
if [ "${BYTES}" != "123" ]; then
  errorexit "bytesize"
fi

BYTES=$(bytesize "1M" "BYTES")
if [ "${BYTES}" != "${MIB}" ]; then
  errorexit "bytesize"
fi

action "bytesize error check:"
BYTES=$(bytesize "abc" "size")
if [ "${?}" = 0 ]; then
  errorexit "bytesize"
fi

INFO=$(sizeinfo 100000)
if [ "${INFO}" != "100000 bytes" ]; then
  errorexit "sizeinfo"
fi

INFO=$(sizeinfo 999999)
if [ "${INFO}" != "999999 bytes" ]; then
  errorexit "sizeinfo"
fi

INFO=$(sizeinfo 1000000)
if [ "${INFO}" != "0.9 MiB" ]; then
  errorexit "sizeinfo"
fi

INFO=$(sizeinfo 1048575)
if [ "${INFO}" != "0.9 MiB" ]; then
  errorexit "sizeinfo"
fi

INFO=$(sizeinfo 1048576)
if [ "${INFO}" != "1 MiB" ]; then
  errorexit "sizeinfo"
fi

INFO=$(sizeinfo 1048577)
if [ "${INFO}" != "1.1 MiB" ]; then
  errorexit "sizeinfo"
fi

INFO=$(sizeinfo 2097151)
if [ "${INFO}" != "1.9 MiB" ]; then
  errorexit "sizeinfo"
fi

INFO=$(sizeinfo 2097152)
if [ "${INFO}" != "2 MiB" ]; then
  errorexit "sizeinfo"
fi

INFO=$(sizeinfo 2097153)
if [ "${INFO}" != "2.1 MiB" ]; then
  errorexit "sizeinfo"
fi

printf '%s' "abc123" > "${TESTPATH}/test.txt"
SPACE=$(filespace "${TESTPATH}/test.txt")
exitonerror ${?}
note "File space for \"abc123\" = ${SPACE} bytes"
if [ "${SPACE}" = "" ]; then
  errorexit "filespace"
fi
if [ "${SPACE}" -lt 6 ]; then
  errorexit "filespace"
fi

printf '%s' "abc123" > "${TESTPATH}/test.txt"
SIZE=$(filesize "${TESTPATH}/test.txt")
exitonerror ${?}
note "File size for \"abc123\" = ${SIZE} bytes"
if [ "${SIZE}" != "6" ]; then
  errorexit "filesize"
fi

#-------------------------------------------------
separator "filesystem"

ABSPATH="$(abspath ".")"
exitonerror ${?}
note "abspath: ${ABSPATH}"
if [ "${ABSPATH}" != "$(pwd)" ]; then
  errorexit "abspath"
fi

TRUEPATH="$(truepath ".")"
exitonerror ${?}
note "truepath: ${TRUEPATH}"
if [ ! -f "${TRUEPATH}/${SCRIPT}" ]; then
  errorexit "truepath"
fi

rm -rf "${TESTPATH}/test"
ensuredir "${TESTPATH}/test"
exitonerror ${?}
if [ ! -d "${TESTPATH}/test" ]; then
  errorexit "ensuredir"
fi
ensuredir "${TESTPATH}/test"
exitonerror ${?}
rmdir "${TESTPATH}/test"

ensurenofile "${TESTPATH}/test.txt"
clearfile "${TESTPATH}/test.txt"
exitonerror ${?}
if [ ! -f "${TESTPATH}/test.txt" ]; then
  errorexit "clearfile"
fi
SPACE=$(filespace "${TESTPATH}/test.txt")
exitonerror ${?}
if [ "${SPACE}" -ge 16384 ]; then
  errorexit "clearfile"
fi
SIZE=$(filesize "${TESTPATH}/test.txt")
exitonerror ${?}
if [ "${SIZE}" != 0 ]; then
  errorexit "clearfile: expected 0 bytes, got ${SIZE}"
fi

ensurenofile "${TESTPATH}/test.txt"
exitonerror ${?}
clearfile "${TESTPATH}/test.txt" ${MIB}
exitonerror ${?}
if [ ! -f "${TESTPATH}/test.txt" ]; then
  errorexit "clearfile"
fi
SPACE=$(filespace "${TESTPATH}/test.txt")
exitonerror ${?}
if [ "${SPACE}" -ge 16384 ]; then
  note "Trying to create sparse file with 1 MiB, got $(sizeinfo ${SPACE})"
fi
SIZE=$(filesize "${TESTPATH}/test.txt")
exitonerror ${?}
if [ "${SIZE}" != "${MIB}" ]; then
  errorexit "clearfile"
fi

ensurenofile "${TESTPATH}/test.txt"
exitonerror ${?}
ensurefile "${TESTPATH}/test.txt"
exitonerror ${?}
if [ ! -f "${TESTPATH}/test.txt" ]; then
  errorexit "ensurefile"
fi

ensurefile "${TESTPATH}/test.txt"
exitonerror ${?}
ensurenofile "${TESTPATH}/test.txt"
exitonerror ${?}
if [ -f "${TESTPATH}/test.txt" ]; then
  errorexit "ensurenofile"
fi

ensurenofile "${TESTPATH}/test.txt"
exitonerror ${?}
clearfile "${TESTPATH}/test.txt"
exitonerror ${?}
inheritowner "${TESTPATH}/test.txt" sudo
exitonerror ${?}

#-------------------------------------------------
separator "saveline"

ensurenofile "${TESTPATH}/test.txt"
saveline "abc" "${TESTPATH}/test.txt"
exitonerror ${?} "saveline1"
DATA="$(loadline "${TESTPATH}/test.txt")"
if [ "${DATA}" != "abc" ]; then
  errorexit "writeline1"
fi

echo "123" > "${TESTPATH}/test.txt"
saveline "abc" "${TESTPATH}/test.txt"
exitonerror ${?} "saveline2"
DATA="$(cat "${TESTPATH}/test.txt")"
if [ "${DATA}" != "abc" ]; then
  errorexit "writeline2"
fi

appendline "xyz" "${TESTPATH}/test.txt"
exitonerror ${?} "appendline1"
DATA="$(cat "${TESTPATH}/test.txt" | head -1)"
if [ "${DATA}" != "abc" ]; then
  errorexit "appendline2"
fi
DATA="$(cat "${TESTPATH}/test.txt" | head -2 | tail -1)"
if [ "${DATA}" != "xyz" ]; then
  errorexit "appendline3"
fi

ensurenofile "${TESTPATH}/test.txt"
appendline "xyz" "${TESTPATH}/test.txt"
exitonerror ${?} "appendline4"
DATA="$(cat "${TESTPATH}/test.txt")"
if [ "${DATA}" != "xyz" ]; then
  errorexit "appendline5"
fi

clearfile "${TESTPATH}/test.txt"
appendline "123" "${TESTPATH}/test.txt"
exitonerror ${?} "appendline6"
DATA="$(cat "${TESTPATH}/test.txt")"
if [ "${DATA}" != "123" ]; then
  errorexit "appendline7"
fi

#-------------------------------------------------
separator "ensurecopy"

ensuredir "${TESTPATH}/copydir"
ensurenofile "${TESTPATH}/copydir/test.txt"

ensurecopy "${TESTPATH}/test.txt" "${TESTPATH}/copydir"
exitonerror ${?} "ensurecopy1"
cmp -s "${TESTPATH}/test.txt" "${TESTPATH}/copydir/test.txt"
exitonerror ${?} "ensurecopy2"

appendline "345" "${TESTPATH}/test.txt"
ensurecopy "${TESTPATH}/test.txt" "${TESTPATH}/copydir/test.txt"
exitonerror ${?} "ensurecopy3"
cmp -s "${TESTPATH}/test.txt" "${TESTPATH}/copydir/test.txt"
exitonerror ${?} "ensurecopy4"

appendline "567" "${TESTPATH}/copydir/test.txt"
ensurecopy "${TESTPATH}/test.txt" "${TESTPATH}/copydir/"
exitonerror ${?} "ensurecopy5"
cmp -s "${TESTPATH}/test.txt" "${TESTPATH}/copydir/test.txt"
exitonerror ${?} "ensurecopy6"

ensurecopy "${TESTPATH}/test.txt" "${TESTPATH}/copydir/test.txt"
exitonerror ${?} "ensurecopy7"

sleep 1

touch "${TESTPATH}/copydir/test.txt"
ensurecopy "${TESTPATH}/test.txt" "${TESTPATH}/copydir/test.txt"
exitonerror ${?} "ensurecopy8"

#-------------------------------------------------
separator "is"

isint "0a"
if [ "${?}" = 0 ]; then
  errorexit "isint"
fi

isint "a0"
if [ "${?}" = 0 ]; then
  errorexit "isint"
fi

isint "0+"
if [ "${?}" = 0 ]; then
  errorexit "isint"
fi

isint "+0"
if [ "${?}" != 0 ]; then
  errorexit "isint"
fi

isint "-0"
if [ "${?}" != 0 ]; then
  errorexit "isint"
fi

isint "123"
if [ "${?}" != 0 ]; then
  errorexit "isint"
fi

isint "-123"
if [ "${?}" != 0 ]; then
  errorexit "isint"
fi

isint "123.1"
if [ "${?}" = 0 ]; then
  errorexit "isint"
fi

iscmd "ls"
if [ "${?}" != 0 ]; then
  errorexit "iscmd"
fi

iscmd "dklfhjdklvnldfkhfjkhnkjs"
if [ "${?}" = 0 ]; then
  errorexit "iscmd"
fi

#-------------------------------------------------
# separator "scriptlock"

scriptlock
exitonerror ${?}

note "locked"

#-------------------------------------------------
separator "check"

action "checkint error check:"
checkint "123.1"
if [ "${?}" = 0 ]; then
  errorexit "checkint allowed float"
fi

checkint "-123"
if [ "${?}" != 0 ]; then
  errorexit "checkint"
fi

checkint "+0"
if [ "${?}" != 0 ]; then
  errorexit "checkint"
fi

action "checkdevice error check:"
checkdevice "${LOGFILE}"
if [ "${?}" = 0 ]; then
  errorexit "checkdevice"
fi

#-------------------------------------------------
separator "end"
