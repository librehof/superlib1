#!/bin/sh
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
SUPERPATH="${SCRIPTPATH}/../bin"
. "${SUPERPATH}/superlib"
cd "${SCRIPTPATH}"

#-------------------------------------------------

# superlib1 | test-httpdownload

summary()
{
  info
  info "Test superlib's httpdownload"
  info
  info "Usage:"
  info " ${SCRIPT}"
  info
}

#-------------------------------------------------

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit
fi

TESTPATH="${RUNPATH}/superlib"
rm -rf "${TESTPATH}"
ensuredir "${TESTPATH}"
ensurelog "${TESTPATH}/test.log"
ensurenofile "${TESTPATH}/test.var"

div

note "SUPERLIB=${SUPERLIB}"
note "TESTPATH=${TESTPATH}"
note "LOGFILE=${LOGFILE}"

WANT="superlib"

#-------------------------------------------------
separator "plain download: found"

httpdownload "https://gitlab.com/librehof/superlib1/-/raw/dev/meta/label.var" "${TESTPATH}/test.var"
exitonerror ${?}

HAVE="$(head -1 "${TESTPATH}/test.var")"
if [ "${WANT}" != "${HAVE}" ]; then
  errorexit "Expected content \"${WANT}\", got \"${HAVE}\""
fi

#-------------------------------------------------
separator "plain download: not found"

httpdownload "https://gitlab.com/librehof/superlib1/-/raw/dev/meta/unknown.var" "${TESTPATH}/test.var"
if [ "${?}" = 0 ]; then
  errorexit "Expected download to fail"
fi

if [ -f "${TESTPATH}/test.var" ]; then
  errorexit "File still exists: ${TESTPATH}/test.var"
fi

#-------------------------------------------------
separator "download with md5: match"

httpdownload "https://gitlab.com/librehof/superlib1/-/raw/dev/meta/label.var" "${TESTPATH}/test.var" a2e55cfe9ea46be1cbf4f2db117a3599
exitonerror ${?}

#-------------------------------------------------
separator "download with md5: mismatch"

httpdownload "https://gitlab.com/librehof/superlib1/-/raw/dev/meta/version.var" "${TESTPATH}/test.var" a2e55cfe9ea46be1cbf4f2db117a3599
if [ "${?}" = 0 ]; then
  errorexit "Expected hash to fail"
fi

#-------------------------------------------------
separator "download with sha1: match"

httpdownload "https://gitlab.com/librehof/superlib1/-/raw/dev/meta/label.var" "${TESTPATH}/test.var" 3a334c7884b6aac04bf52f69d2d1453d388b7513
exitonerror ${?}

#-------------------------------------------------
separator "download with sha1: mismatch"

httpdownload "https://gitlab.com/librehof/superlib1/-/raw/dev/meta/version.var" "${TESTPATH}/test.var" 3a334c7884b6aac04bf52f69d2d1453d388b7513
if [ "${?}" = 0 ]; then
  errorexit "Expected hash to fail"
fi

#-------------------------------------------------
separator "end"
