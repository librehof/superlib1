#!/bin/sh
SCRIPT="$(basename "${0}")"
SCRIPTPATH="$(dirname "${0}")"
. "${SCRIPTPATH}/superlib"
# cd "${SCRIPTPATH}"

#-------------------------------------------------

# somecommand * something.com

summary()
{
  info
  info "Doing something"
  info
  info "Usage:"
  info " ./${SCRIPT}"
  info
}

#-------------------------------------------------

if [ "${1}" = "-h" ] || [ "${1}" = "--help" ]; then
  summaryexit
fi

#ensurelog "${SCRIPTPATH}/${SCRIPTNAME}.log"

#scriptlock
#exitonerror ${?}

#ensurecmd somecommand @ apk:apkpackagename somegeneralpackagename
#exitonerror ${?}

#-------------------------------------------------

action "Doing something"

# do something

success "Done something"

#-------------------------------------------------
