### Superlib 1 | changelog

Apache 2.0 (C) 2023 [librehof.org](http://librehof.org)

2023-11-02 (v1.11)
- will reset GRANDDEPTH when shifting from superuser to user (runuser)

2023-09-20 (v1.10)
- REMOVED: filelock
- REMOVED: USERID
- REMOVED: RUNPREFIX
- REMOVED: CMDLINE and the cmdline.log feature
- added: UID
- changed: RUNPATH=/run\[/...\]/super, not inherited
- changed: TMPPATH=/tmp/super
- added: SCRIPTPID=$$
- added: superunlock
- added: scriptlock/scriptunlock, with auto unlock/unmount
- added: SCRIPTLOCK=\<lockfile\>, inherited
- added: RUNMOUNT=\<lockmount\>
- superpm.day moved to /tmp/super/pm.day

2023-08-30 (v1.7)
- separator function now uses note (will log)
- success function no longer calls sync
- ensurecmd and ensurepkg now supports end-of-line comment (#)
- SUPERDIV renamed to DIV
- SUPERCPA renamed to SUPERAUTO

2023-08-27 (v1.6)
- ensurecmd will try "first package" a second time if command is still missing
  - helps if a secondary package is found but does not contain the command

2023-03-26 (v1.5)
- repo renamed to superlib1
- ensurecopy will preserve time, mode bits and links
- httpdownload with insecure option (no longer default)
- genpass12 and genpass24

2023-03-18 (v1.4)
- cp with reflink=auto
- ensurecopy: if equal content, will ensure mtime is equal without using cp
- utcmonth and hostmonth
- abspath will replace initial ~ with HOME (if HOME is not blank) 

2023-02-25 (v1.3)
- ensurecopy lowercase action text
- dir-replicate error message

2022-10-02 (v1.2)
- inherit includes both userid and groupid (instead of just userid)
- coding.md fix

2022-05-12 (v1.1)
- secretinput

2022-04-02 (v1.0)
- ensurenodir
- inheritowner user and group
