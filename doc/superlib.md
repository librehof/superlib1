## Superlib 1 | superlib

Apache 2.0 (C) 2023 [librehof.org](http://librehof.org)

Library for run-anywhere scripts

### Categories

- <tt>I</tt>
  - [Time](#time)
  - [Key stroke](#key-stroke)
  - [String functions](#string-functions)
  - [Find in string](#find-in-string)
  - [Field functions](#field-functions)
  - [Replace functions](#replace-functions)
  - [Write line](#write-line)
  - [File size](#file-size)
  - [File-system functions](#file-system-functions)
  - [Lock](#lock)
  - [Hash functions](#hash-and-id-functions)
  - [Test functions](#test-functions)
  - [Check functions with error message](#check-functions-with-error-message)

- <tt>II</tt>
  - [Download and Installation](#download-and-installation)

- <tt>III</tt>
  - [General logging](#general-logging)
  - [General reporting](#general-reporting)
  - [Action reporting](#action-reporting)
  - [Exit and reporting](#exit-and-reporting)
  - [Large script reporting](#large-script-reporting)


### Import

- import from same directory (superlib copied to your script's location):
  ```
  #!/bin/sh
  SCRIPT="$(basename "${0}")"
  SCRIPTPATH="$(dirname "${0}")"
  . "${SCRIPTPATH}/superlib"
  # cd "${SCRIPTPATH}"
  ```

- import from another directory (relative to script):
  ```
  #!/bin/sh
  SCRIPT="$(basename "${0}")"
  SCRIPTPATH="$(dirname "${0}")"
  SUPERPATH="${SCRIPTPATH}/<relative path>"
  . "${SUPERPATH}/superlib"
  # cd "${SCRIPTPATH}"
  ```

- system wide import:
  ```
  #!/bin/sh
  SCRIPT="$(basename "${0}")"
  SCRIPTPATH="$(dirname "${0}")"
  . "/bin/superlib"
  # cd "${SCRIPTPATH}"
  ```


### English only

- Error messages in "easy" english
- Will set a neutral locale (en_US.UTF-8 or C.UTF-8)


### Elevation (access level)

 | Level         | Condition                                               |
 |:--------------|:--------------------------------------------------------|
 | **user**      | user access, UID != 0                                   |
 | **superuser** | system access, UID = 0                                  |


### Locality

 | Locality        | Condition                                             |
 |:----------------|:------------------------------------------------------|
 | **userlocal**   | started as ordinary **user**, GRANDUID != 0<br>sub-scripts may run userlocal with system access, <br>use: sudo **-E** \<sub-script\> in parent script|
 | **systemwide**  | started as **superuser**, GRANDUID = 0                |

<br>
<hr>

### Configurable variables

- non-inherited:

  | variable    | value (default)                                            |
  |:------------|:-----------------------------------------------------------|
  | SUPERPATH   | "*SCRIPTPATH*", "/bin" or ""                               |
  | SCRIPTUNIT  | defaults to script's leaf directory                        |
  | SCRIPTNAME  | name of script (without .sh)                               |

- inherited

  | variable    | value (default)                                            |
  |:------------|:-----------------------------------------------------------|
  | TMPPATH     | "/tmp/super"                                               |
  | LOGFILE     | "" (see ensurelog)                                         |
  | LOGLIMIT    | 100000 (log rotation limit in bytes)                       |
  | TIMEOUT     | 5 (general time-out in seconds)                            |
  | SUPERPM     | auto-detected: "apt", "yum", "dnf", "pacman", "zypper", "apk" or "" |
  | SUPERAUTO   | "true" (false disables reflink=auto in ensurecopy)         |
  | DIV         | "true"                                                     |

### Non-configurable variables

- assigned before import

  | variable    | value                                                      |
  |:------------|:-----------------------------------------------------------|
  | SCRIPTPATH  | script's directory path (made absolute by superlib)        |
  | SCRIPT      | filename of script                                         |

- non-inherited:

  | variable    | value                                                      |
  |:------------|:-----------------------------------------------------------|
  | RUNPATH     | "/run\[/user/*GRANDUID*\]/super"                           |
  | RUNMOUNT    | see scriptlock                                             |
  | STDFILE     | "*RUNPATH*/stdfile.*SCRIPTPID*"                            |
  | UID         | id -u                                                      |
  | USER        | id -u -n                                                   |
  | MIB         | 1048576 (1024*1024)                                        |
  | IFS         | newline (can be saved/restored)                            |
  | TTYSTRING   | tty config restored on exit                                |
  | SUPERLIB    | *version*                                                  |

- inherited:
  
  | variable    | value                                                      |
  |:------------|:-----------------------------------------------------------|
  | GRANDDEPTH  | 1..20 (1=grand-script)                                     |
  | GRANDPID    | process id of grand-script = process group id (pgid)       |
  | GRANDPATH   | grand-script's *SCRIPTPATH*                                |
  | GRANDNAME   | grand-script's *SCRIPTNAME*                                |
  | GRANDSCRIPT | grand-script's filename                                    |
  | GRANDUID    | *UID* of user that launched grand-script                   |
  | GRANDUSER   | **userlocal**: \<*logged in user*\> (even if elevated sub-script) <br>**systemwide**: \<*superuser*\> (usually "root") |
  | HOSTNAME    | machine label                                              |
  | LOGDAY      | $(utcday) if *LOGFILE*                                     |
  | SCRIPTLOCK  | lockfile (when locked)                                     |

<br>
<hr>

### Special files

- | file                           | comment                                         |
  |:-------------------------------|:------------------------------------------------|
  | /run/super/super.lock          | short system exclusive lock (see superlock)     |
  | /run/super/super.mnt           | short temporary mount (see superlock)           |
  | *RUNPATH*/\<name\>.lock        | named lock (see scriptlock)                     |
  | /run/super/\<name\>.mnt        | named temporary mount (see scriptlock)          |
  | *RUNPATH*/stdfile.*SCRIPTPID*  | temporary stderr capture file (removed on exit) |
  | *RUNPATH*/\<any\>.*SCRIPTPID*  | small temporary file (removed on exit)          |
  | /tmp/super/pm.day              | utc day of last package-manager refresh         |

<br>
<hr>

### Time

#### Global time

```YEAR=$(utcyear)```
- "yyyy"

```MONTH=$(utcmonth)```
- "yyyymm"

```DAY=$(utcday)```
- "yyyymmdd"

```TIME="$(utctime ["<format>"])```
- "yyyymmddUhhmmss"

#### Host time

```YEAR=$(hostyear)```
- "yyyy"

```MONTH="$(hostmonth)```
- "yyyymm"

```DAY="$(hostday)```
- "yyyymmdd"

```TIME="$(hosttime ["<format>"])```
- "yyyymmddThhmmss"

<br>
<hr>

### Key stroke

```KEY="$(keyinput [timeout])"```
- outputs key or blank
- optional timeout in seconds (max 25)

```keywait [timeout]```
- waits for key
- optional timeout in seconds (max 25)

```secretinput ["prompt"]```
- default prompt: "Password:"
- SECRET=\<input\>

<br>
<hr>

### String functions

```LEN=$(stringlen "<string>")```

```START="$(startstring "<string>" 1..n)"```

```END="$(endstring "<string>" 1..n)"```

```PART="$(substring "<string>" 1..n 1..n)"```

```UPPER="$(upperstring "<string>")"```

```LOWER="$(lowerstring "<string>")"```

<br>
<hr>

### Find in string

```findforward "<string>" "<searchsubstring>" [1..n]```
- start=1 or ${3}
- pos=\<1-based position before substring\>
- found="$(substring "\<string\>" \<start\> \<pos\>)"
- return: 0=found, 1=not found

```findbackward "<string>" "<searchseparator>" [1..n]```
- findbackward only works with single-char separator (!)
- pos=\<1-based position after separator\>
- end=stringlen or ${3}
- found="$(substring "\<string\>" \<pos\> \<end\>)"
- return: 0=found, 1=not found

<br>
<hr>

### Field functions

```FIELD="$(spacefield "<string>" <1..n>)"```
- pick space-separated field in string

```FIELD="$(tabfield "<string>" <1..n>)"```
- pick tab-separated field in string

<br>
<hr>

### Replace functions

```NEWSTR="$(replaceinstring "<from>" "<to>" "<string>")"```

```replaceinfile "<from>" "<to>" "<file>" [sudo]```
- error: message with return 2

<br>
<hr>

### Write line

```LINE="$(loadline "<file>")"```
- read single-line file
- error: message with return 2

```saveline "<string>" "<file>" [sudo]```
- newline is added to string, then written to new file
- error: message with return 2

```appendline "<string>" "<file>" [sudo]```
- newline is added to string, then appended to file
- file will be created if missing  
- error: message with return 2

<br>
<hr>

### File size

```INFO=$(sizeinfo <bytes>)```
- if n \< 1000000
  - "n bytes"
- elif n is a multiple of MIB:
  - "m MiB"
- else:
  - "m.n MiB" (cut to one decimal)

```BYTES=$(bytesize "<bytes>|<MiB>M" ["<varname>"])```
- output size in bytes
- error: message with return 2

```BYTES=$(filespace "<file>")```
- occupied file space on disk, in bytes
- could be zero, even for a large file (sparse file)
- error: 0

```BYTES=$(filesize "<file>")```
- logical file size, in bytes
- error: 0

<br>
<hr>

### File-system functions

```PATH="$(abspath "<path>")"```
- absolute path
- parent must exist
- error: message with return 2

```PATH="$(truepath "<path>")"```
- resolve links and output absolute path
- error: message with return 2

```ensuredir "<dir>" [<mode>]```
- parent must exist
- error: message with return 2
- on creation and superuser: 
  - owner = owner of parent dir

```ensurenodir "<dir>" [sudo] [note]```
- error: message with return 2

```ensurefile "<file>" [sudo]```
- directory must exist
- error: message with return 2  
- on creation and superuser: 
  - owner = owner of parent dir

```clearfile "<file>" [<sparsesize>[M]] [sudo]```
- directory must exist
- error: message with return 2  
- if superuser:
  - owner = owner of parent dir

```ensurenofile "<file>" [sudo] [note]```
- error: message with return 2

```ensurecopy "<source>" "<target>" [sudo]```
- ensure file is same as source based on size and modification time
- will preserve time, mode bits and link, but not chattr and xattr
- invokes inheritowner (applies reasonable ownership logic if superuser)
- uses reflink=auto unless SUPERCPA="false"
- error: message with return 2

```inheritowner "<target>" [sudo]```
- will be silently ignored if ordinary user
- if superuser/sudo, and \<target\> is owned by superuser:
  - will change owner and/or group to that of parent directory of target
- error: message with return 2

```ensurelog "<logfile>"```
- LOGFILE=\<logfile\>
- log to syslog if LOGFILE="#\<systag\>"  
- error: LOGFILE="", message with return 2

```clearlog "<logfile>"```
- ensurelog \<logfile\>
- clearfile \<logfile\>
- error: LOGFILE="", message with return 2

<br>
<hr>

### Lock

```scriptlock "<name>" [timeout]```
- default name = SCRIPTNAME
- timeout in seconds (TIMEOUT)
- only one lock at a time
- recursive lock via SCRIPTLOCK (RUNMOUNT is not recursive)
- runlock="${RUNPATH}/\<name\>.lock" (unlocked on exit)
- RUNMOUNT="/run/super/\<name\>.mnt" (unmounted on exit)
- timeout/error: message with return 2

```scriptunlock```
- unlock if runlock
- unmount if RUNMOUNT exists (superuser only)

```superlock [timeout]```
- checksuperuser
- scriptlock super \<timeout\>
- timeout/error: message with return 2

```superunlock```
- scriptunlock

<br>
<hr>

### Hash and ID functions

```HASH="$(md5hash "<file>")"```
- outputs 128-bit lowercase MD5 hex (32 characters)
- error: message with return 2

```HASH="$(sha1hash "<file>")"```
- outputs 160-bit lowercase SHA-1 hex (40 characters)
- error: message with return 2

```ID="$(genhexid)"```
- outputs 128-bit random lowercase hex (32 characters)
- error: message with return 2

```PASSWD="$(genpass12)"```
- outputs random 12-character password (for humans)

```PASSWD="$(genpass24)"```
- outputs random 24-character password (for machines)
- safe from brute-force methods
- error: message with return 2

<br>
<hr>

### Test functions

```isint "<possible integer>"```
- return: 0=true, 1=false

```iscmd "<system command>"```
- return: 0=true, 1=false

<br>
<hr>

### Check functions with error message

```checkint "<possible integer>" ["<varname>"]```
- check if integer
- false/error: message with return 2

```checkfile "<path>"```
- check if file exists
- false/error: message with return 2

```checkdir "<path>"```
- check if directory exists
- false/error: message with return 2

```checkdevice "<block device>"```
- check if block device is present
- will give device 1 second to appear
- false/error: message with return 2

```checkcmd "<command>"```
- check command is available
- false/error: message with return 2

```checksuperuser```
- check if superuser
- false/error: message with return 2

<br>
<hr>

### Download and Installation

```httpdownload "<url>" "<path>" ["<md5/sha1>"]```
- curl is used for download (will ensure it is installed)
- error: message with return 2

```refreshpm```
- refresh package manager's index
- failure: warning with return 1
- error: message with return 2

```ensurepkg [<pm>:]<package>[+] ...```
- takes a list of package names (to cover multiple package managers)
- see [command-not-found.com](https://command-not-found.com) for package names
- success: return 0, package="\<package\>" if installed
- error: message with return 2

```ensurecmd "<command>" @ [<pm>:]<package>[+] ...```
- takes a command + list of package names (to cover multiple package managers)
- see [command-not-found.com](https://command-not-found.com) for package names
- success: return 0, package="\<package\>" if installed
- error: message with return 2

<br>
<hr>

### General logging

```filelog "<line>" "<logfile>" [<bytelimit>]```
- directory must exist, silent failure otherwise
- will copy "\<logfile\>" to "\<logfile\>.old" on rotation

```printlog "<line(s)>"```
- print to stderr (fd2) and log to LOGFILE (if set)
- log to syslog if LOGFILE="#\<systag\>"
- only print if LOGFILE=""

<br>
<hr>

### General reporting

```info ["<message>"]```
- send info/progress message to stderr (fd2)
- not logged, to be viewed in a shell "when it happens"
 
```note "<message>"```
- printlog "\<message\>"

```notetail "<file>" [maxlines]```
- default maxlines=5
- if file exists:
  - printlog last line(s)

```warning "<message>"```
- printlog "WARNING: \<message\>"

```error "<message>"```
- printlog "ERROR: \<message\>"

<br>
<hr>

### Action reporting

```action "Doing something"```
- printlog "\<message\>"

```success ["Done something"]```
- calls sync if grand script (writes buffered data to file)
- printlog "ok" or "\<message\>"

<br>
<hr>

### Exit and reporting

```exit```or ```exit 0```
- success exit (default exit)

```exit 1```
- none/false/warning exit

```summaryexit ["<impact>"]```
- before calling: define summary() function with info lines
- impact = "" | "read" | "bind" | "write" | "read/write" | ...
1. info "\<SCRIPT\> (\<impact\>)"
2. summary()
3. exit 99

```inputexit```
- info "\<command line\>"
- error "Illegal input (try --help)"
- exit 3

```errorexit ["<message>"]```
- \[error \<message\>]
- exit 2

```exitonerror <status> ["<message>"]```
- if status \>= 1:
  - \[error \<message\>]
  - exit 2

<br>
<hr>

### Large script reporting

```title ["<title>"]```
- note "#\[#...\] \<title\>"
- number of # = *GRANDDEPTH*
- default title = *SCRIPTNAME*
    
```separator ["<label>"]```
- if DIV="true":
  - div
  - note "\<50-char separator with optional label\>"
  - div
- else: 
  - title 

```div```
- if DIV="true":
  - blank info line

<br>
<hr>
